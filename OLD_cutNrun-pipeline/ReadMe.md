# README 
cut-n-run-pipeline branch cmu_dev 

## Quick Protocol:
1. Make a directory called "Fastq/" in your project directory.  
2. Copy fastq.gz files that are going to processed into this directory.  
3. Copy following files from the pipeline directory into your working directory:
	1. cut-n-run-pipeline/runSnakemake.sh
	2. cut-n-run-pipeline/snakefile_cmu_mod.snakemake.py
	3. *optional* ClusterConfig/config_CnR_snake.json
4. Create or modify a samplesheet (see "Samplesheet" for details)
5. Open config_CnR_snake.json and change the "sampleSheetPath" field to the name of samplesheet you are using.
6. Run "module load python". Requires >= python/3.5
7. *Optional* Perform a dry-run by running, "run snakemake --printshellcmds -nrs -snakefile_CMU_mod.snakemake.py"
8. Run "bash runSnakemake.sh snakefile_CMU_mod.snakemake.py"
9. ???
10. Profit.

## Directory Structure:

### Required Directory Structure Before Beginning:

MyProject/  
├── cut-n-run-pipeline/   
│   ├── ClusterConfig  
│   ├── __pycache__   
│   ├── Src  
│   └── UnUsed_Scripts  
├── Fastq/   
├── mySampleSheet.csv   
└── runSnakemake.sh  

### Final Directory Structure:  

MyProject/  
├── Bam  
├── Bed  
├── BigWig  
├── cut-n-run-pipeline  
│   ├── ClusterConfig  
│   ├── __pycache__  
│   ├── Src  
│   └── UnUsed_Scripts  
├── Err_Out  
├── Fastq  
├── FastQC  
├── Logs  
├── multiqc_data  
├── Peaks  
├── Plots  
└── Sam  

## Samplesheet:

### Description:
The samplesheet has two required fields, "fastq" and "techName". All other fields are optional.  
"fastq" refers to the name of the original fastq file and "techName" is used to both rename files and combine technical replicates.  
fastq files with the same "techName" are concatenated together. "techName" should not have file extensions or read number information.

### Examples:  

**fastq**  
CMU08_10-BAK27R_3LW_H33B_Sup_Rep1_U10_GACCTGAA-TTGGTGAG_S1_L001_R1_001.fastq.gz  
CMU08_10-BAK27R_3LW_H33B_Sup_Rep1_U10_GACCTGAA-TTGGTGAG_S1_L001_R2_001.fastq.gz  
CMU08_10-BAK27R_3LW_H33B_Sup_Rep1_U10_GACCTGAA-TTGGTGAG_S1_L002_R1_001.fastq.gz  
CMU08_10-BAK27R_3LW_H33B_Sup_Rep1_U10_GACCTGAA-TTGGTGAG_S1_L002_R2_001.fastq.gz  

**techName**  
BAK27R_3LW_H33B_Sup_Rep1_U10  
BAK27R_3LW_H33B_Sup_Rep1_U10  
BAK27R_3LW_H33B_Sup_Rep1_U10  
BAK27R_3LW_H33B_Sup_Rep1_U10  

When running the pipeline, the following commands will be run:  
cat Fastq/CMU08_10-BAK27R_3LW_H33B_Sup_Rep1_U10_GACCTGAA-TTGGTGAG_S1_L001_R1_001.fastq.gz Fastq/CMU08_10-BAK27R_3LW_H33B_Sup_Rep1_U10_GACCTGAA-TTGGTGAG_S1_L002_R1_001.fastq.gz > Fastq/BAK27R_3LW_H33B_Sup_Rep1_U10_R1.fastq.gz  
cat Fastq/CMU08_10-BAK27R_3LW_H33B_Sup_Rep1_U10_GACCTGAA-TTGGTGAG_S1_L001_R2_001.fastq.gz Fastq/CMU08_10-BAK27R_3LW_H33B_Sup_Rep1_U10_GACCTGAA-TTGGTGAG_S1_L002_R2_001.fastq.gz > Fastq/BAK27R_3LW_H33B_Sup_Rep1_U10_R2.fastq.gz  

This should generate the following files:    
Fastq/BAK27R_3LW_H33B_Sup_Rep1_U10_R1.fastq.gz  
Fastq/BAK27R_3LW_H33B_Sup_Rep1_U10_R2.fastq.gz  

## Pipeline Structure:

├── ClusterConfig    
│   ├── config_CnR_snake.json  
│   ├── killdevilConfig.json  
│   ├── killdevilConfig.yaml  
│   ├── slurmConfig.json  
│   └── slurmConfig.yaml  
├── makeMergedFiles.snakemake.py  
├── __pycache__  
│   ├── py_sam_2_spikenormbg.cpython-33.pyc  
│   ├── py_sam_2_spikenormbg.cpython-35.pyc  
│   └── py_sam_2_spikenormbg.cpython-36.pyc  
├── ReadMe.md  
├── runSnakemake.sh  
├── sampleSheet_191206.csv  
├── sampleSheet.csv  
├── snakefile_CMU_mod.snakemake.py  
├── Src  
│   ├── countReads.sh  
│   ├── getUnmapped.awk  
│   ├── makeFragmentSizePlots.R  
│   ├── parseflagstat.py  
│   ├── spikeNorm.py  
│   ├── splitFrags.awk  
│   ├── splitPosNeg.awk  
│   └── zNorm.r  
└── UnUsed_Scripts  
    ├── cutrun.sh  
    ├── makeMergedFiles_with_bamFiles.snakemake.py  
    ├── mergeFileConfig_bamFiles.json  
    ├── mergeFileConfig.csv  
    ├── py_sam_2_spikenormbg.py  
    ├── Snakefile  
    └── spikeInTesting.snakemake.py  



/bin/bash: ../tree: No such file or directory
