#!/usr/bin/bash


module purge && module load samtools/1.9

echo 'chr	length	mapped	unmapped	prefix' > summaryBamReadNum.tsv

fileList=($(ls Bam/*_sorted.bam))
for ((i=0; i<${#fileList[@]}; i++)); 
do
	fileName=${fileList[$i]}
	prefixName=${fileName##*/}
	if [[ ! -f ${fileName}.bai ]]; 
	then	
		samtools index ${fileName}
	fi
	echo ${fileName}
	samtools idxstats ${fileName} | sed "s/.*/&	${prefixName}/" >> summaryBamReadNum.tsv
done



#fileList=($(ls Bam/*bam))
#for ((i=0; i<${#fileList[@]}; i++)); 
#do
#	fileName=${fileList[$i]}
#	prefixName=${fileName##*/}
#
#	if [[ ${fileName} =~ "_sorted.bam" ]]; 
#	then
#		:
#	elif [[ ! -f ${fileName%%.bam}_sorted.bam.bai ]];
#	then
#		echo "here!"
#		samtools sort -@ 6 ${fileName} > ${fileName%%.bam}_sorted.bam
#		samtools index ${fileName%%.bam}_sorted.bam
#		fileName=${fileName%%.bam}_sorted.bam
#	fi
#	#fileName=${fileName%%.bam}_sorted.bam
#	echo ${fileName}
#	samtools idxstats ${fileName} | sed "s/.*/&	${prefixName}/" >> summaryBamReadNum.tsv
#
#	#readCount=$(samtools view -c ${fileName})
#	#echo "${fileName},bam,${readCount}" >> {output.bam}
#done

echo "sample	type	reads" > summaryFastqNum.tsv

fastqList=($(ls Fastq/*R1*trim*fastq*))

for ((i=0; i<${#fastqList[@]}; i++)); 
do	
	fastq=${fastqList[$i]}
	prefixName=${fastq##*/}
	lineCount=$(zcat ${fastq} | wc -l)
	readCount=$(echo "${lineCount}/4" | bc)
	echo "${fastq}	fastq	${readCount}" >> summaryFastqNum.tsv
done

