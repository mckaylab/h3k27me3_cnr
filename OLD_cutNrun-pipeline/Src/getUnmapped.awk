# getUnmapped.awk

# COMMAND LINE ARGUMENT:
#samtools view -h {input} | awk -f {params.srcDirectory}/getUnmapped.awk Bam/{output.qFilter} 

# EXPECTED OUTPUT:
#'Bam/MergedBam/{sample}_20to120.sam'
#'Bam/MergedBam/{sample}_150to700.sam'
#'Bam/MergedBam/{sample}_all.sam'

BEGIN {
	FS="\t"
	if ( ARGC < 2 ){
		print "Usage awk -f getUnmapped.awk <unMappedReads.fastq> [<mapped_file>]";
		exit 1
	} else if (ARGC == 3){
		unMappedFile=ARGV[1];
		mappedFile=ARGV[2];
		print mappedFile;
		delete ARGV
	} else if (ARGC == 2){
		unMappedFile=ARGV[1];
		mappedFile="";
		delete ARGV;
	} else { 
		print "FAILURE!";
		exit 1;
	}
}
		#print mappedFile;
		#print "here";
		#print unMappedFile;


{ if ($0 ~ "^@" && mappedFile != ""){
	print $0 > mappedFile ".sam";
} else if ($0 ~ "^@" && mappedFile == ""){
	print $0;
} else if (($3 != "*") && ($5 >= 5) && (mappedFile != "" )){ 
	print $0 > mappedFile ".sam";
} else if (($3 != "*") && ($5 >= 5) && (mappedFile == "" )){
	print $0;
} else 
	print "@"$1"\n"$10"\n+\n"$11 > unMappedFile ".fastq";
}

#/^@/ { 
#	print $0;
#	next;
#}
#
#{ if (($3 != "*") && ($5 >= 5) && (mappedFile != "" )){ 
#	print $0 > mappedFile ".sam";
#} else if (($3 != "*") && ($5 >= 5) && (mappedFile == "" )){
#	print $0;
#} else 
#	print "@"$1"\n"$10"\n+\n"$11 > unMappedFile ".fastq";
#}

#	if (mappedFile != ""){
#		print $0 > mappedFile ".sam";
#		next;
#	} else if (mappedFile == ""){
#		print $0 > mappedFile ".sam";
#	}



#{ if (($3 != "*") && ($5 >= 5) ){
#	print $0 > mappedFile ".sam";
#	}

# TLEN stored in 9th field of sam file	
# TLEN can be positive or negative depending on fragment orientation
#{ if (($9 > 20 && $9 < 120  ) || ( $9 < -20 && $9 > -120 )){
#	#print "hey"
#	print $0 > outPrefix "_20to120.sam";
#	print $0 > outPrefix "_all.sam"
#	}
#else if (( $9 > 150 && $9 < 700 ) || ( $9 < -150 && $9 > -700 )){
#	#print "hey"
#	print $0 > outPrefix "_150to700.sam"
#	print $0 > outPrefix "_all.sam"
#	}
#else 
#	#print "hey"
#	print $0 > outPrefix "_all.sam"
#}
