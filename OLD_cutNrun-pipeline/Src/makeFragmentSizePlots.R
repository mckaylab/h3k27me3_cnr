readNarrowPeakAsGranges = function(narrowFile, metaColumns = 'default', fileNameAsMeta = NULL){
  require(GenomicRanges)
  readNarrowPeakAsGranges.description = 
    "\n readNarrowPeakAsGranges(narrowFile):
  - Import macs2 narrow peak file as a GRanges object
  - Input: 
  narrowfile: string w/ path to narrrow file
  metaColumns: vector w/ custom column names. Useful if columns have been cut out.
  - Output: 
  GRanges object
  - Accepts any variation of bed file now, including custom ones, as long as columns 1:3 are chr, start, end, name
  - All other columns are assigned as meta columns
  \n"
  ## Check that function inputs are correct
  if (narrowFile == 'help'){ cat(readNarrowPeakAsGranges.description); return() }
  else if (!file.exists(narrowFile)){ print('File does not exist!'); cat(readNarrowPeakAsGranges.description); return();}
  else {
    ## Set meta column names
    if (metaColumns == 'default'){
      metaColumns = c('chrom', 'start', 'end', 
                      'name', 'intScore', 'strand', 'foldChange',
                      'negLog10pVal', 'negLog10qVal', 'relSummitPos')
    }
    ## Read in narrow peak file
    narrowTable = try(read.table(narrowFile))
    if (inherits(narrowTable, 'try-error')){
      return(NULL)
    }
    colnames(narrowTable) = metaColumns

    ## Convert to GRanges
    if ('strand' %in% metaColumns){
      narrowGRange = makeGRangesFromDataFrame(narrowTable, keep.extra.columns = T, seqnames.field = 'chrom', start.field = 'start', end.field = 'end', strand.field = 'strand')
    }
    else {
      narrowGRange = makeGRangesFromDataFrame(narrowTable, keep.extra.columns = T, seqnames.field = 'chrom', start.field = 'start', end.field = 'end')
    }
    # add file name as meta column
    if(!is.null(fileNameAsMeta)){
      narrowFileName = gsub(basename(narrowFile), pattern = '(.*)\\.*', replacement = '\\1')
      mcols(narrowGRange)['fileName'] = narrowFileName
    }
    return(narrowGRange)
  }
}

extendBySummit = function(narrowPeak, width = 200, summitField = 'relSummitPos'){
  narrowPeak@ranges@start = narrowPeak@ranges@start + narrowPeak@elementMetadata[[summitField]]
  narrowPeak = resize(narrowPeak, width = 1, fix = 'start')
  narrowPeak = resize(narrowPeak, width = width, fix = 'center')
  return(narrowPeak)
}

calculateStats = function(bedFile, peakFile, frags, peaks){
  peakNum = length(peaks)
  fragNum = length(frags)
  fragInPeaks = findOverlaps(frags, peaks)
  fragInPeaksNum = length(unique(queryHits(fragInPeaks)))
  frIP = (fragInPeaksNum/fragNum) * 100
  statTable = data.frame(bedFile = bedFile,
                     peakFile = peakFile,
                     peakNumber = peakNum,
                     fragmentNumber = fragNum,
                     fragmentsInPeaks = fragInPeaksNum,
                     FrIP = sprintf('%.2f%%', frIP))
  fragInPeaks = frags[queryHits(fragInPeaks),]
  return(list(statTable = statTable, fragInPeaks = fragInPeaks))
}

########################################

# PARSE ARGUMENTS
print('blue')

args = commandArgs(trailingOnly = T)

if (length(args) < 2){
  stop('\nHi There!\nYou have the wrong Number of Arguments.\nUsage: <bedFile> <peakFile> [plot directory] [stat directory]')
} else if(length(args) >= 3){
  plotDir = args[3]
  if(!grepl(plotDir, pattern = '.*/$' == T)){
    plotDir = paste0(plotDir, '/')
  } 
  if (length(args) == 4){
    statDir = args[4]
    if (!grepl(statDir, pattern = '.*/$')){
      statDir = paste0(statDir, '/')
    }
  }
} else{
  plotDir = './'
}

bedFile = args[1]
peakFile = args[2]

baseName.bed = gsub(bedFile, pattern = '.*/(.*)\\..*', replacement = '\\1')

# bedFile='./Beds+Bigs+Peaks/E93-CUTnRUN-100wing-10min_rep1.fragment.bed'
# peakFile='./Beds+Bigs+Peaks/E93-mimic-ChIP-Rep12-Pooled_peaks.narrowPeak'

# LOAD DEPENDENCIES
library(ggplot2)
library(rtracklayer)
library(magrittr)

# IMPORT FILES
#frags = import.bed(bedFile)

frags = readr::read_delim(bedFile, delim = '\t', col_names = c('chr', 'start', 'end')) %>%
   GRanges()
    
peaks = readNarrowPeakAsGranges(peakFile)
if (!is.null(peaks)){
   peaks = peaks[order(peaks@elementMetadata$negLog10qVal, decreasing = T),]
   peaks = extendBySummit(peaks)
   stats.fragInPeaks = calculateStats(bedFile, peakFile, frags, peaks)
   stats = stats.fragInPeaks[[1]]

   # calculate statistics
   fragInPeaks = stats.fragInPeaks[[2]]

   # write statistics table
   write.table(x = stats, file = paste0('Logs/', baseName.bed, '_statFile.csv'), sep = ',', row.names = F)
   frags@elementMetadata['Peak.Status'] = NA
   frags@elementMetadata$Peak.Status[frags %in% fragInPeaks == T] = 'In.Peaks'
   frags@elementMetadata$Peak.Status[frags %in% fragInPeaks == F] = 'Not.In.Peaks'
} else{
   frags@elementMetadata$Peak.Status = 'Not.In.Peaks'
}



# split fragments into whether in peaks or not in peaks
# calculate fragment size distribution for all fragments, frags in and frags outside peaks
frags = data.frame(frags)
fragCountsAll = dplyr::count(frags, width)

fragCountsSplit = split(frags, frags$Peak.Status)
fragCountsSplit = lapply(fragCountsSplit, function(x){
  x = dplyr::count(x, width)
  return(x)
})
fragCountsSplit = dplyr::bind_rows(fragCountsSplit, .id = 'id')

# PLOT DATA
plotTheme = theme(panel.background = NULL, panel.grid.major = element_line(colour = 'grey60'), panel.grid.minor = element_line(color = 'grey80'))

fragCountsAll.plot = ggplot(fragCountsAll, aes(x = width, y = n)) +
  geom_bar(stat = 'identity', width = 1) + 
  scale_x_continuous(breaks = function(x){print(x[1]); breaks = seq(0, x[2], by = 20); return(breaks)}) +
  plotTheme

fragCountsSplit.plot = ggplot(fragCountsSplit, aes(x = width, y = n)) +
  geom_bar(stat = 'identity', aes(fill = id), width = 1) +
  facet_wrap(~id, nrow = 2, scales = 'free') + 
  scale_x_continuous(breaks = function(x){print(x[1]); breaks = seq(0, x[2], by = 20); return(breaks)}) +
  plotTheme

fragCountsAll.plot

# SAVE FRAGMENTS PLOTS
#png(paste0(plotDir, baseName.bed, '_fragmentDistribution--allReads.png'), width = 2000, height = 1000)
png(sprintf('%s/%s_fragmentDist--allReads.png', plotDir, baseName.bed), width = 2000, height = 1000)
print(fragCountsAll.plot)
dev.off()

#png(paste0(plotDir, baseName.bed, '_fragmentDistribution--peaks.png'), width = 2000, height = 1000)
png(sprintf('%s/%s_fragmentDist--peaks.png', plotDir, baseName.bed), width = 2000, height = 1000)
print(fragCountsSplit.plot)
dev.off()

# CALCULATE CUMULATIVE DISTRIBUTION
cumulVals = seq(1, max(frags$width), by = 1)
fragWidth.cumulDist = ecdf(frags$width)
fragWidth.cumulDist = fragWidth.cumulDist(cumulVals)
fragWidth.cumulDist = data.frame(width = cumulVals, freq = fragWidth.cumulDist)

# PLOT CUMULATIVE DISTRIBUTION
cumulDist.plot = ggplot(fragWidth.cumulDist, aes(x = width , y = freq)) + geom_line() + 
  scale_x_continuous(breaks = seq(0, max(cumulVals), by = 20)) + scale_y_continuous(breaks = seq(0, 1, by = 0.1)) + 
  plotTheme

# SAVE CUMULATIVE DISTRIBUTION PLOT
#png(paste0(plotDir, baseName.bed, '_cumulativeDistributionPlot.png'), width = 1000, height = 1000)
png(paste0(plotDir, baseName.bed, '_cumulativeDistPlot.png'), width = 2000, height = 1000)
print(cumulDist.plot)
dev.off()
