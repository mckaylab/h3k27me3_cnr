# splitFrags.awk by cuyehara

# COMMAND LINE ARGUMENT:
#samtools view -h {input} | awk -f {params.srcDirectory}/splitFrags.awk Bam/{wildcards.sample}

# EXPECTED OUTPUT:
#'Bam/MergedBam/{sample}_20to120.sam'
#'Bam/MergedBam/{sample}_150to700.sam'
#'Bam/MergedBam/{sample}_all.sam'


BEGIN {
	FS="\t"
	if (ARGV[1]){
		outPrefix=ARGV[1];
		ARGC--
		#print "test" > outPrefix "_pos_internal--fragEnd.bed"
	}
	else {
		print "No prefix supplied!"
		exit 1
	}
}


/^@/ { 
	print $0 > outPrefix "_20to120.sam";
	print $0 > outPrefix "_150to700.sam";
	print $0 > outPrefix "_all.sam";
	next 
}

# TLEN stored in 9th field of sam file	
# TLEN can be positive or negative depending on fragment orientation
{ if (($9 > 20 && $9 < 120  ) || ( $9 < -20 && $9 > -120 )){
	#print "hey"
	print $0 > outPrefix "_20to120.sam";
	print $0 > outPrefix "_all.sam"
	}
else if (( $9 > 150 && $9 < 700 ) || ( $9 < -150 && $9 > -700 )){
	#print "hey"
	print $0 > outPrefix "_150to700.sam"
	print $0 > outPrefix "_all.sam"
	}
else 
	#print "hey"
	print $0 > outPrefix "_all.sam"
}

