# splitPosNeg.awk by CUyehara

# Snakemake command: 
#cat {input} | awk -f srcDirectory/splitPosNeg.awk Bed/MergedBed/FragEnd/{wildcards.sample}

BEGIN { 
	FS="\t" 
	if (ARGV[1]){
		outPrefix=ARGV[1];
		ARGC--
		#print "test" > outPrefix "_pos_internal--fragEnd.bed"
	}
	else {
		print "No prefix supplied!"
		exit 1
	}
}

{ if($6 ~ /+/ && ($2-1 > 0 )){
	print $1"\t"$2"\t"$2+1"\t"$4"\t"$5"\t"$6 > outPrefix "_internal_pos.bed";
	print $1"\t"$2"\t"$2+1"\t"$4"\t"$5"\t"$6 > outPrefix "_internal_all.bed";
	print $1"\t"$2-1"\t"$2"\t"$4"\t"$5"\t"$6 > outPrefix "_external_pos.bed";
	print $1"\t"$2-1"\t"$2"\t"$4"\t"$5"\t"$6 > outPrefix "_external_all.bed" ;
	}
else if ($6 ~ /+/ && ($2-1 < 0)){
	print $1"\t"$2"\t"$2+1"\t"$4"\t"$5"\t"$6 > outPrefix "_internal_pos.bed";
	print $1"\t"$2"\t"$2+1"\t"$4"\t"$5"\t"$6 > outPrefix "_internal_all.bed";
	}
else if ($6 ~ /-/ ){
	print $1"\t"$3-1"\t"$3"\t"$4"\t"$5"\t"$6 > outPrefix "_internal_neg.bed";
	print $1"\t"$3"\t"$3+1"\t"$4"\t"$5"\t"$6 > outPrefix "_external_neg.bed";
	print $1"\t"$3-1"\t"$3"\t"$4"\t"$5"\t"$6 > outPrefix "_internal_all.bed";
	print $1"\t"$3"\t"$3+1"\t"$4"\t"$5"\t"$6 > outPrefix "_external_all.bed";
	}
else
	next;
}
