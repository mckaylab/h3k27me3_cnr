import pandas as pd

import os
import shutil
import re
import glob
import sys
import getpass
import argparse

#configfile: './cut-n-run-pipeline/ClusterConfig/config_CnR_snake.json'

if not os.path.isfile('config_CnR_snake.json'):
	shutil.copy('cut-n-run-pipeline/ClusterConfig/config_CnR_snake.json', './')
	
configfile: 'config_CnR_snake.json'

##############################
# SAMPLE FILE CONFIG

#sampleDF = pd.read_csv('sampleSheet.csv', comment = '#').set_index('mergeName', drop = False)
#sampleDF = pd.read_csv('sampleSheet_updated.csv', comment = '#').set_index('mergeName', drop = False)
#print(sampleSheetPath)

#sampleDF = pd.read_csv(config['sampleSheetPath'], comment = '#').set_index('mergeName', drop = False)
sampleDF = pd.read_csv(config['sampleSheetPath'], comment = '#')

print(sampleDF)

def getTechReplicates(wildcards):
	#techFilter = sampleDF[sampleDF.techName == '{tech}_R{Num}_001'.format(**wildcards)]
	#print(type(wildcards.Num))
	#readNumRegex = '_R{}_001'.format(wildcards.Num)
	readNumRegex = '_R{}'.format(wildcards.Num)

	techFilter = sampleDF [ sampleDF.techName == wildcards.tech ]
	fastqList = list(techFilter.fastq)
	#fastqList = [ fastq for fastq in fastqList if re.search(wildcards.Num, fastq) ] 
	fastqList = [ fastq for fastq in fastqList 
			if re.search(readNumRegex, fastq) ]
	fastqList = [ 'Fastq/{}'.format(fastq) for fastq in fastqList ]
	#print(fastqList)
	return(fastqList)
	
def getMergedBed(wildcards):
	print('start')
	print(vars(wildcards))
	mergeFilter = sampleDF [ sampleDF.mergeName == wildcards.merge ]
	techList = list(mergeFilter.techName)
	techList = [ re.sub('_R[12]_001', '', t) for t in techList ] 
	techList = [ 'Bed/{tech}_dm6_trim_q5_dupsRemoved.bed'.format(tech = techName, **wildcards) # species = wildcards.species, fragType = wildcards.fragType) 
			 for techName in techList ] 
	print(techList)
	print('end')
	#mergeBedList = [ 'Bed/{mergeName}_dm6_trim_q5_dupsRemoved_{fragType}.bed'.format(mergeName = mergeName, **wildcards) 
	#print(mergeBedList)
	return(techList)

def getSampleList(techList):
	sampleList = set()
	for tech in techList:
		#strip "_repX" from end of tech string
		sampleList.add(tech[0:-5])
	return(sampleList)

#######################################
# DEFINE REFERENCE AND SPIKE GENOMES:

#refGenome = 'dm'
#spikeGenome = 'sacCer'
#
#speciesList = [refGenome, spikeGenome]

#speciesList = indexDict.keys()
speciesList = ['dm6']

fragTypeList = ['all', '20to120', '150to700']
normTypeList = ['', '_spikeNorm', '_rpgcNorm']

localrules: all

###################################
# CREATE LISTS FOR USE IN RULE ALL

#print(sampleDF)

techList =  set(sampleDF.techName)
fastqList = list(set(sampleDF.fastq))
sampleList = getSampleList(techList)
tech_lookup = sampleDF[['techName','poolName']].drop_duplicates()
print(techList)

##############################
# SNAKEMAKE RULES

rule all:
	input:
		expand('Fastq/{fastq}', fastq = fastqList),
		expand('Fastq/{tech}_R{Num}.fastq.gz', tech = techList, Num = ['1', '2'] ),
		expand('Fastq/{tech}_R{Num}_trim.fastq.gz', tech = techList, Num = ['1', '2']),
		expand('Bam/{tech}_dm6_trim_sorted.bam', tech = techList),
		expand('Bam/{tech}_dm6_trim_q5_sorted.bam', tech = techList),
		expand('Bam/{tech}_dm6_trim_q5_dupsMarked_sorted.bam', tech = techList),
		expand('Bam/{tech}_dm6_trim_q5_dupsRemoved_sorted.bam', tech = techList),
		expand('Bam/{tech}_dm6_trim_q5_dupsRemoved_sorted.bam.bai', tech = techList),
		expand('Bed/{tech}_dm6_trim_q5_dupsRemoved_{fragType}.bed', fragType = fragTypeList, tech = techList ),
		expand('BigWig/{tech}_dm6_trim_q5_dupsRemoved_{fragType}.bw', fragType = fragTypeList, tech = techList),
		expand('BigWig/{tech}_dm6_trim_q5_dupsRemoved_{fragType}_rpgcNorm_zNorm.bw', fragType = fragTypeList, tech = techList), 
		expand('Bam/{tech_pooled}_dm6_repsPooled_trim_q5_dupsRemoved_sorted.bam', tech_pooled = tech_lookup.poolName), 
		expand('Bam/{tech_pooled}_dm6_repsPooled_trim_q5_dupsRemoved_sorted.bam.bai', tech_pooled = tech_lookup.poolName),
		expand('BigWig/{tech_pooled}_dm6_repsPooled_trim_q5_dupsRemoved_sorted_rpgcNorm.bw', tech_pooled = tech_lookup.poolName),
		expand('BigWig/{tech_pooled}_dm6_repsPooled_trim_q5_dupsRemoved_sorted_rpgcNorm_zNorm.bw', tech_pooled = tech_lookup.poolName),
		expand('Peaks/{tech}_dm6_trim_q5_dupsRemoved_{fragType}_peaks.narrowPeak', fragType = fragTypeList, tech = techList),
		expand('Plots/{tech}_dm6_trim_q5_dupsRemoved_{fragType}_cumulativeDistPlot.png', fragType = fragTypeList, tech = techList),
		expand('FastQC/{tech}_R1_trim_screen.txt', tech = techList),
		expand('FastQC/{tech}_R1_trim_fastqc.html', tech = techList), 
		expand('multiqc_data/multiqc_{qcType}.txt', qcType = [ 'fastqc', 'fastq_screen', 'general_stats', 'sources']),
		'summaryBamReadNum.tsv',
		'summaryFastqNum.tsv'

rule moveFiles:
	input:
		lambda x: list(sampleDF.htsfFile)
	output:
		expand('Fastq/{fastq}', fastq = list(sampleDF.fastq))
	run:
		for htsf in list(sampleDF.htsfFile):
			print(wildcards)
			outFileFilt = sampleDF [ sampleDF.htsfFile == htsf ]
			outFileBase = list(outFileFilt.fastq)[0]
			outFile = f'Fastq/{outFileBase}'
			print(outFile)
			shutil.copy(htsf, outFile)
			print('copied file')



#	shell:
#		"""
#		inputList=({input})
#		for ((i=0; i<${{#inputList[@]}}; i++)); 
#		do
#			cp ${{inputList[$i]}} Fastq/
#		done
#		"""


rule combineTechReps:
	input:
		getTechReplicates
	output:
		'Fastq/{tech}_R{Num,[12]}.fastq.gz'
	shell:
		"""
		cat {input} > {output}
		"""

rule adapter_trim_reads:
	input:
		readOne = 'Fastq/{tech}_R1.fastq.gz',
		readTwo = 'Fastq/{tech}_R2.fastq.gz'
	output:
		readOne = 'Fastq/{tech}_R1_trim.fastq.gz',	
		readTwo = 'Fastq/{tech}_R2_trim.fastq.gz',
		adapterStats = 'Logs/{tech}_adapterStats',
		trimStats = 'Logs/{tech}_trimStats'
	params:
		module = config['module']['bbmapVer']
	shell:
		"""
		module purge && module load {params.module}
		bbduk.sh in1={input.readOne} in2={input.readTwo} out1={output.readOne} out2={output.readTwo} stats={output.adapterStats} ktrim=r ref=adapters rcomp=t tpe=t tbo=t hdist=1 mink=11 > {output.trimStats}
		"""

#rule bbsplit:
#	input:
#		readOne = 'Fastq/{tech}_R1_trim.fastq.gz',
#		readTwo = 'Fastq/{tech}_R2_trim.fastq.gz'
#	output:
#		expand('Fastq/{{tech}}_dm6_trim.fastq', species = speciesList)
#	params:
#		refGenomePath = ','.join( [ indexDict[species]['fasta'] for species in indexDict.keys() ] ),
#		module = config['module']['bbmapVer']
#		#module = bbmapVer
#
#	threads: 8
#	log:
#		'Logs/{tech}_bbsplit.txt'
#	shell:
#		"""
#		module purge && module load {params.module}
#		bbsplit.sh in={input.readOne} in2={input.readTwo} ref={params.refGenomePath} basename=Fastq/{wildcards.tech}_%_trim.fastq threads={threads} ambiguous2=best refstats={log}
#		"""

rule align_all:
	input:
		readOne = 'Fastq/{align}_R1_trim.fastq.gz',
		readTwo = 'Fastq/{align}_R2_trim.fastq.gz'
	output:
		temp('Sam/{align}_dm6_trim.sam')
	params:
		module = config['module']['bowtie2Ver'],
		bowtieIndex = config['index']['dm6']['bowtie']
		#module = bowtie2Ver,
		#bowtieIndex = indexDict['dm6']['bowtie'] 
		#bowtieIndex = lambda wildcards: indexDict[wildcards.species]['bowtie']
	threads: 8
	log:
		'Logs/{align}_dm6_bowtieStats.txt'
	shell:
		"""
		module purge && module load {params.module}
		bowtie2 -p {threads} -q --local --very-sensitive-local --no-mixed --no-discordant --phred33 -I 5 -X 999 -x {params.bowtieIndex} -1 {input.readOne} -2 {input.readTwo} -S {output} 2> {log}
		"""
		#bowtie2 -p {threads} -q --local --very-sensitive-local --no-mixed --no-discordant --phred33 -I 5 -X 999 -x {params.bowtieIndex} --interleaved {input} -S {output} 2> {log}

rule convertToBam_and_qFilter:
	input:
		'Sam/{tech}_dm6_trim.sam'
	output:
		bam = 'Bam/{tech}_dm6_trim_sorted.bam',
		bamIndex = 'Bam/{tech}_dm6_trim_sorted.bam.bai',
		qFilter = 'Bam/{tech}_dm6_trim_q5_sorted.bam',
		qFilterIndex = 'Bam/{tech}_dm6_trim_q5_sorted.bam.bai'

	params:
		module = config['module']['samtoolsVer']
		#module = samtoolsVer
	threads: 4
	log:
		'Logs/{tech}_dm6_trim.flagstat'
	shell:
		"""
		module purge && module load {params.module}
		samtools sort -@ 4 -O BAM {input} > {output.bam}
		samtools index {output.bam}
		samtools view -@ 4 -bq 5 {output.bam} | samtools sort -@ 4 -O BAM > {output.qFilter}
		samtools index {output.qFilter}
		"""

rule markDups:
	input:
		'Bam/{tech}_dm6_trim_q5_sorted.bam'
	output:
		markedDups = temp('Bam/{tech}_dm6_trim_q5_dupsMarked.bam'),
		PCRdups = 'Logs/{tech}_dm6_trim_PCR_duplicates.txt',
		markedDupsSorted = 'Bam/{tech}_dm6_trim_q5_dupsMarked_sorted.bam', 
		index = 'Bam/{tech}_dm6_trim_q5_dupsMarked_sorted.bam.bai',
	params:
		picardPath = config['module']['picardPath'],
		picardModule = config['module']['picardVer'],
		samModule = config['module']['samtoolsVer'], 
		#picardPath = picardPath,
		#picardModule = picardVer,
		#samModule = samtoolsVer, 
	shell:
		"""
		module purge && module load {params.picardModule}
		java -Xmx16g -jar {params.picardPath} MarkDuplicates INPUT= {input} OUTPUT= {output.markedDups} METRICS_FILE= {output.PCRdups} REMOVE_DUPLICATES= false ASSUME_SORTED= true

		module purge && module load {params.samModule}
		samtools sort -@ 4 {output.markedDups} > {output.markedDupsSorted}
		samtools index {output.markedDupsSorted}
		"""

rule removeDups:
	input:
		'Bam/{tech}_dm6_trim_q5_dupsMarked_sorted.bam' 
	output:
		bam = 'Bam/{tech}_dm6_trim_q5_dupsRemoved_sorted.bam',
		index = 'Bam/{tech}_dm6_trim_q5_dupsRemoved_sorted.bam.bai' 
	params:
		module = config['module']['samtoolsVer']
	threads: 8
	shell:
		"""
		module purge && module load {params.module}
		samtools view -@ {threads} -bF 0x400 {input} | samtools sort -@ 4 > {output.bam} 
		samtools index {output.bam}
		"""

rule convertBamToBed:
	input:
		'Bam/{tech}_dm6_trim_q5_dupsRemoved_sorted.bam'
	output:
		nameSort = temp('Bam/{tech}_dm6_trim_q5_dupsRemoved_nameSorted.bam'),
		bed = 'Bed/{tech}_dm6_trim_q5_dupsRemoved.bed'
	params:
		bedModule = config['module']['bedtoolsVer'],
		samModule = config['module']['samtoolsVer']
	threads: 4
	shell:
		"""
		module purge && module load {params.samModule}
		samtools sort -@ {threads} -n {input} -o {output.nameSort}

		module purge && module load {params.bedModule}
		bedtools bamtobed -bedpe -i {output.nameSort} | sort -k 1,1 -k 2,2n > {output.bed}		
		"""

rule splitFragments:
	input:
		'Bed/{sample}_dm6_trim_q5_dupsRemoved.bed'
	output:
		allFrags = 'Bed/{sample}_dm6_trim_q5_dupsRemoved_all.bed',
		smallFrags = 'Bed/{sample}_dm6_trim_q5_dupsRemoved_20to120.bed',
		bigFrags = 'Bed/{sample}_dm6_trim_q5_dupsRemoved_150to700.bed'
	shell:
		"""
		cut -f 1,2,6,7 {input} | awk -F '\t' '{{print $0, ($3-$2)}}' - > {output.allFrags}
		awk -v OFS='\t' '($5>20) && ($5<120) {{print $0}}' {output.allFrags} > {output.smallFrags}
		awk -v OFS='\t' '($5>150) && ($5<700) {{print $0}}' {output.allFrags} > {output.bigFrags}
		"""
	
rule makeBigWig:
	input:
		dm = 'Bed/{sample}_dm6_trim_q5_dupsRemoved_{fragType}.bed',
	output:
		unNorm = 'BigWig/{sample}_dm6_trim_q5_dupsRemoved_{fragType}.bw',
		rpgcNorm = 'BigWig/{sample}_dm6_trim_q5_dupsRemoved_{fragType}_rpgcNorm.bw'
	params:
		genomeSize = config['general']['genomeSize'],
		chromSizePath = config['general']['chromSizePath'],
		bedModule = config['module']['bedtoolsVer'],
		ucscModule = config['module']['ucscVer'],
		readLen = config['general']['readLen']
	shell:
		"""
		module purge && module load {params.bedModule} {params.ucscModule}
		readCount=$(wc -l {input.dm} | sed -e 's/^  *//' -e 's/  */,/g' | cut -d , -f 1)

		rpgcScale=$(echo "scale=5; {params.genomeSize}/(${{readCount}} * {params.readLen})" | bc)
		bedtools genomecov -i {input.dm} -bga -g {params.chromSizePath} | wigToBigWig stdin {params.chromSizePath} {output.unNorm}
		bedtools genomecov -i {input.dm} -bga -g {params.chromSizePath} -scale ${{rpgcScale}} | wigToBigWig stdin {params.chromSizePath} {output.rpgcNorm}
		"""

rule zNormBigWig:
	input:
		'BigWig/{sample}_dm6_trim_q5_dupsRemoved_{fragType}_rpgcNorm.bw'
	output:
		zNorm = 'BigWig/{sample}_dm6_trim_q5_dupsRemoved_{fragType}_rpgcNorm_zNorm.bw',
		zStats = 'Logs/{sample}_dm6_trim_q5_dupsRemoved_{fragType}.zNorm'
	params:
		module = config['module']['rVer'],
		srcDirectory = config['general']['srcDirectory']
	shell:
		"""
		module purge && module load {params.module}
		Rscript --vanilla {params.srcDirectory}/Src/zNorm.r {input} {output.zNorm} > {output.zStats}
		"""

rule callPeaks:
	input:
		'Bed/{sample}_dm6_trim_q5_dupsRemoved_{fragType}.bed'
	output:
		'Peaks/{sample}_dm6_trim_q5_dupsRemoved_{fragType}_peaks.narrowPeak'
	params:
		module = config['module']['macsVer'],
		control = config['general']['controlDNAPath'],
		prefix = 'Peaks/{sample}_dm6_trim_q5_dupsRemoved_{fragType}',
		genomeSize = config['general']['genomeSize']
	shell:
		"""
		module purge && module load {params.module}
		macs2 callpeak -f BEDPE -c {params.control} --broad -n {params.prefix} -g {params.genomeSize} -t {input} --nomodel --seed 123
		macs2 callpeak -f BEDPE -c {params.control} -n {params.prefix} -g {params.genomeSize} -t {input} --nomodel --seed 123 
		"""

rule makeFragmentSizePlots:
	input:
		bed = 'Bed/{sample}_dm6_trim_q5_dupsRemoved_{fragType}.bed',
		peaks = 'Peaks/{sample}_dm6_trim_q5_dupsRemoved_{fragType}_peaks.narrowPeak'
	output:
		'Plots/{sample}_dm6_trim_q5_dupsRemoved_{fragType}_cumulativeDistPlot.png'

	params:
		module = config['module']['rVer'],
		srcDirectory = config['general']['srcDirectory']
	shell:
		"""
		module purge && module load {params.module}
		Rscript --vanilla {params.srcDirectory}/Src/makeFragmentSizePlots.R {input.bed} {input.peaks} Plots/
		"""

rule countReadNum:
	input: 
		expand('Bam/{tech}_dm6_trim_sorted.bam', tech = techList, species = speciesList),
		expand('Bam/{tech}_dm6_trim_q5_sorted.bam', tech = techList, species = speciesList), 
		expand('Bam/{tech}_dm6_trim_q5_dupsMarked_sorted.bam', tech = techList, species = speciesList), 
		expand('Bam/{tech}_dm6_trim_q5_dupsRemoved_sorted.bam', tech = techList, species = speciesList),
	output:
		bamSummary = 'summaryBamReadNum.tsv',
		fastqSummary = 'summaryFastqNum.tsv'
	params:
		module = config['module']['samtoolsVer'],
		srcDirectory = config['general']['srcDirectory']
	shell:
		"""
		module purge && module load {params.module}
		{params.srcDirectory}/Src/countReads.sh
		"""

rule fastQC:
	input: 
		'Fastq/{sample}_R1_trim.fastq.gz'
	output:
		#'FastQC/{sample}_R{Num}{trim}_fastqc.html'
		'FastQC/{sample}_R1_trim_fastqc.html'
	params:
		module = config['module']['fastqcVer']
	shell:
		"""
		module purge && module load {params.module}

		fastqc -o ./FastQC/ -f fastq {input}
		"""

rule fastqScreen:
	input:
		'Fastq/{sample}_R1_trim.fastq.gz'
	output:
		txt = 'FastQC/{sample}_R1_trim_screen.txt',
		html = 'FastQC/{sample}_R1_trim_screen.html'
	params:
		screenPath = config['module']['screenPath'],
		screenConf = config['module']['screenConf'],
	shell:
		"""
		{params.screenPath} --aligner bowtie2 -conf {params.screenConf} {input} --outdir ./FastQC/
		"""

rule multiQC:
	input:
		bam = expand('Bam/{tech}_dm6_trim_q5_dupsRemoved_sorted.bam', tech = techList, species = speciesList),
		qc = expand('FastQC/{tech}_R1_trim_fastqc.html', tech = techList, Num = ['1', '2']),
		screen = expand('FastQC/{tech}_R1_trim_screen.html', tech = techList, Num = ['1', '2']),
	output:
		fastqc = 'multiqc_data/multiqc_fastqc.txt',
		screen = 'multiqc_data/multiqc_fastq_screen.txt',
		stats = 'multiqc_data/multiqc_general_stats.txt',
		sources = 'multiqc_data/multiqc_sources.txt'
	params:
		module = config['module']['multiqcVer']
	shell:
		"""
		module purge && module load {params.module}
		multiqc -f . -o ./
		"""

### Attempting to add rule for pooled BW ###
#rule derived from master FAIRE pipeline

rule mergeBams:
	input:
		lambda wildcards : 'Bam/' + tech_lookup[tech_lookup.poolName == wildcards.tech_pooled].techName + '_dm6_trim_q5_dupsRemoved_sorted.bam'
		#expand('Bam/{tech}_dm6_trim_q5_dupsRemoved_sorted.bam', tech = techList) 
	params: 
		moduleVer = config['module']['samtoolsVer']
	output:
		bam = 'Bam/{tech_pooled}_dm6_repsPooled_trim_q5_dupsRemoved_sorted.bam',
		index = 'Bam/{tech_pooled}_dm6_repsPooled_trim_q5_dupsRemoved_sorted.bam.bai',
		#bam = expand("Bam/{tech}_dm6_trim_q5 {dirID}_{nFiles}Reps_POOLED_q5_sorted_dupsRemoved_noYUHet.bam", dirID = dirID, nFiles = nFiles),
		#idx = expand("Bam/{dirID}_{nFiles}Reps_POOLED_q5_sorted_dupsRemoved_noYUHet.bam.bai", dirID = dirID, nFiles = nFiles)
	shell:
		"""
		module purge && module load {params.moduleVer}
		samtools merge {output.bam} {input} &&
		samtools index {output.bam}
		"""

# Bam Coverage to output bigwig file normalized to genomic coverage
# changed removed read extension flag and dropped binSize to 1 to mimic CMU bedtools genomecov in previous steps
# may way to change method to making merged bed files split by frag prior to making bw 

rule mergeBigWig:
	input:
		#bam = lambda wildcards : tech_lookup[tech_lookup.poolName == wildcards.tech_pooled].poolName + '_dm6_repsPooled_trim_q5_dupsRemoved_sorted.bam',
		#index =	lambda wildcards : tech_lookup[tech_lookup.poolName == wildcards.tech_pooled].poolName + '_dm6_repsPooled_trim_q5_dupsRemoved_sorted.bam.bai'
		bam = 'Bam/{tech_pooled}_dm6_repsPooled_trim_q5_dupsRemoved_sorted.bam',
		index = 'Bam/{tech_pooled}_dm6_repsPooled_trim_q5_dupsRemoved_sorted.bam.bai',
		#bam = expand("Bam/{dirID}_{nFiles}Reps_POOLED_q5_sorted_dupsRemoved_noYUHet.bam", dirID = dirID, nFiles = nFiles),
		#idx = expand("Bam/{dirID}_{nFiles}Reps_POOLED_q5_sorted_dupsRemoved_noYUHet.bam.bai", dirID = dirID, nFiles = nFiles)
	output:
		'BigWig/{tech_pooled}_dm6_repsPooled_trim_q5_dupsRemoved_sorted_rpgcNorm.bw',
	threads: 8 
	params:
		genomeSize = config['general']['genomeSize'],
		deepModule = config['module']['deeptoolsVer'],	
		#chromSizePath = config['general']['chromSizePath'],
		#bedModule = config['module']['bedtoolsVer'],
		#ucscModule = config['module']['ucscVer'],
		#readLen = config['general']['readLen']
	shell:
		"""
		module purge && module load {params.deepModule}
		bamCoverage -b {input.bam} -p {threads} --binSize 1 --normalizeUsing RPGC --effectiveGenomeSize {params.genomeSize} --outFileFormat bigwig -o {output}
		"""

rule mergeZNormBigWig:
# Z-Normalize Bigwig Files
	input:
		'BigWig/{tech_pooled}_dm6_repsPooled_trim_q5_dupsRemoved_sorted_rpgcNorm.bw',
	output:
		zNorm = 'BigWig/{tech_pooled}_dm6_repsPooled_trim_q5_dupsRemoved_sorted_rpgcNorm_zNorm.bw',
		zStats = 'logs/{tech_pooled}_repsPooled.zNorm',
	params: 
		#pipePath = '.faire-pipeline', moduleVer = rVer
		module = config['module']['rVer'],
		srcDirectory = config['general']['srcDirectory']
	shell:
		"""
		module purge && module load {params.module}
		Rscript --vanilla {params.srcDirectory}/Src/zNorm.r {input} {output.zNorm} > {output.zStats}
		"""
#Rscript --vanilla {params.pipePath}/zNorm.r {input.bw} {output.zNorm} > {output.zStats}



# ***************************************
# ** OLD CONFIG FILE INFO STARTS HERE
# ***************************************

## GLOBAL VARIABLES: Modify these as necessary. 
## Also - add user name appt variables and push changes to git.
#
#indexDict = {
#	'dm6' : { 
#		'bowtie' : '/proj/mckaylab/genomeFiles/dm6/Bowtie2Index/genome',
#		'fasta' : '/proj/mckaylab/genomeFiles/dm6/dm6.fa'
#	},
#	'ecoli' : { 
#		'bowtie' : '/proj/mckaylab/genomeFiles/Ecoli_K12_DH10B_ENSEMBL/Bowtie2Index/genome',
#		'fasta' : '/proj/mckaylab/genomeFiles/Ecoli_K12_DH10B_ENSEMBL/ecoli.fa',
#	},
#	'sacCer3' : { 
#		'bowtie' : '/proj/mckaylab/genomeFiles/sacCer3/Bowtie2Index/genome',
#		'fasta' : '/proj/mckaylab/genomeFiles/sacCer3/sacCer3.fa',
#	},
#	'droYak2' : {
#		'bowtie' : '/proj/mckaylab/genomeFiles/droYak2/RefGenome/droYak2',
#		'fasta' : '/proj/mckaylab/genomeFiles/droYak2/droYak2.fa'
#	}
#}
#
#chromSizePath = '/proj/mckaylab/genomeFiles/dm6/dm6_chromSizes.txt'
#controlDNAPath = '/proj/mckaylab/genomeFiles/dm6/ControlGenomicDNA/ControlGenomicDNA_trim_q5_dupsRemoved.bed'
#
#srcDirectory = './cut-n-run-pipeline/'
#
#genomeSize = '137547960'
#
#readLen = '50'
#
###############################
## Module Versions:
#
##bbmapVer = 'bbmap/38.41'
#bbmapVer = 'bbmap/38.67'
#bowtie2Ver = 'bowtie2/2.3.4.1'
#samtoolsVer = 'samtools/1.9'
#
#picardVerNum = '2.2.4'
#picardVer = 'picard/{verNum}'.format(verNum = picardVerNum) 
#picardPath = '/nas/longleaf/apps/picard/{}/picard-tools-{}/picard.jar'.format(picardVerNum, picardVerNum)
#
#bedtoolsVer = 'bedtools/2.26'
#ucscVer = 'ucsctools/320'
#macsVer = 'macs/2.1.2'
#
#rVer = 'r/3.5.2'
#
#fastqcVer = 'fastqc/0.11.7'
#multiqcVer = 'multiqc/1.7'
#
#pythonVer = 'python/3.6.6'
#
#screenPath = '/proj/mckaylab/genomeFiles/fastq_screen_v0.11.1/fastq_screen'
#screenConf = '/proj/mckaylab/genomeFiles/fastq_screen.conf'

# ***************************************
# ** OLD CONFIG FILE INFO ENDS HERE
# ***************************************


################################
# GENERATE SAMPLE LIST:

#sampleList = []
#
#fastqList = glob.glob('./Fastq/*_R[0-9]_001.fastq.gz')
#
#sampleRegex = re.compile('(.*)_R[0-9].*.fastq.gz')
#
#for fastq in fastqList:
#	fastq = os.path.basename(fastq)
#	sample = sampleRegex.match(fastq).group(1)
#	sampleList.append(sample)
#

		#expand('Bed/Merged/{merge}_dm6_trim_q5_dupsRemoved.bed', merge = mergeList, species = speciesList),
		#expand('Bed/Merged/{merge}_dm_trim_q5_dupsRemoved_{fragType}.bed', merge = mergeList, fragType = fragTypeList),
		#expand('BigWig/Merged/{merge}_dm_trim_q5_dupsRemoved_{fragType}{normType}.bw', merge = mergeList, fragType = fragTypeList, normType = normTypeList),
		#expand('BigWig/Merged/{merge}_dm_trim_q5_dupsRemoved_{fragType}_rpgcNorm_zNorm.bw', merge = mergeList, fragType = fragTypeList),
		#expand('Logs/Merged/{merge}_dm_trim_q5_dupsRemoved_{fragType}.zNorm', merge = mergeList, fragType = fragTypeList),
		#expand('Peaks/Merged/{merge}_dm_trim_q5_dupsRemoved_{fragType}_peaks.narrowPeak', merge = mergeList, fragType = fragTypeList),
		#expand('Plots/Merged/{merge}_dm_trim_q5_dupsRemoved_{fragType}_cumulativeDistPlot.png', merge = mergeList, fragType = fragTypeList),


		#expand('Bed/{merge}_dm_trim_q5_dupsRemoved_{fragType}.bed', merge = mergeList, fragType = fragTypeList),
		#expand('BigWig/{merge}_dm_trim_q5_dupsRemoved_{fragType}{normType}.bw', merge = mergeList, fragType = fragTypeList, normType = normTypeList),
		#expand('BigWig/{merge}_dm_trim_q5_dupsRemoved_{fragType}_rpgcNorm_zNorm.bw', merge = mergeList, fragType = fragTypeList),
		#expand('Logs/{merge}_dm_trim_q5_dupsRemoved_{fragType}.zNorm', merge = mergeList, fragType = fragTypeList),
		#expand('Peaks/{merge}_dm_trim_q5_dupsRemoved_{fragType}_peaks.narrowPeak', merge = mergeList, fragType = fragTypeList),
		#expand('Plots/{tech}_dm_trim_q5_dupsRemoved_{fragType}_cumulativeDistPlot.png', tech = techList, fragType = fragTypeList),


		#"Logs/collected_flag_statfiles.csv",
		#'multiqc_data/multiqc_sources.txt'

		#expand('Bed/{sample}_dm6_trim_q5_dupsRemoved.bed', sample = sampleList, species = speciesList),
		#expand('Bed/{sample}_dm_trim_q5_dupsRemoved_{fragType}.bed', sample = sampleList, fragType = fragTypeList),
		#expand('BigWig/{sample}_dm_trim_q5_dupsRemoved_{fragType}{normType}.bw', sample = sampleList, fragType = fragTypeList, normType = normTypeList),
		#expand('BigWig/{sample}_dm_trim_q5_dupsRemoved_{fragType}_rpgcNorm_zNorm.bw', sample = sampleList, fragType = fragTypeList),
		#expand('Logs/{sample}_dm_trim_q5_dupsRemoved_{fragType}.zNorm', sample = sampleList, fragType = fragTypeList),
		#expand('Peaks/{sample}_dm_trim_q5_dupsRemoved_{fragType}_peaks.narrowPeak', sample = sampleList, fragType = fragTypeList),
		##expand('Plots/{merge}_dm_trim_q5_dupsRemoved_{fragType}_cumulativeDistPlot.png', merge = mergeList, fragType = fragTypeList),
		#"Logs/collected_flag_statfiles.csv",
		#'multiqc_data/multiqc_sources.txt'
#rule bbsplit:
#	input:
#		readOne = 'Fastq/{align}_R1_trim.fastq.gz',
#		readTwo = 'Fastq/{align}_R2_trim.fastq.gz'
#	output:
#		dm = 'Fastq/{align}_dm_trim_splitAlign.fastq',
#		droYak = 'Fastq/{align}_droYak_trim_splitAlign.fastq'
#	params:
#		dm = '/proj/mckaylab/genomeFiles/dm6/dm6.fa',
#		droYak= '/proj/mckaylab/genomeFiles/droYak2/RefGenome/droYak2.fa',
#		module = bbmapVer,
#	threads: 8
#	log:
#		'Logs/{align}_bbsplit.txt'
#	shell:
#		"""
#		module purge && module load {params.module}
#		bbsplit.sh in={input.readOne} in2={input.readTwo} ref={params.dm},{params.droYak} out_dm6={output.dm} out_droYak2={output.droYak} ambiguous2=best threads={threads} > {log}
#		"""

#rule qFilter:
#	input:
#		'Bam/{tech}_dm6_trim.bam'
#	output:
#		'Bam/{tech}_dm6_trim_q5.bam'
#	params:
#		module = samtoolsVer
#	#group: "Bam"
#	log:
#		'Logs/{tech}_dm6_trim_q5.flagstat'
#	shell:
#		"""
#		module purge && module load {params.module}
#		samtools view -@ 4 -bq 5 {input} > {output}
#		samtools flagstat {output} > {log}
#		"""

#rule sortBam:
#	input:
#		'Bam/{tech}_dm6_trim_q5_dupsRemoved.bam'
#	output:
#		bam = 'Bam/{tech}_dm6_trim_q5_dupsRemoved_sorted.bam',
#		index = 'Bam/{tech}_dm6_trim_q5_dupsRemoved_sorted.bam.bai'
#	params:
#		module = samtoolsVer
#	threads: 4
#	shell:
#		"""
#		module purge && module load {params.module}
#		samtools sort -@ {threads} -o {output.bam} {input}
#		samtools index {output.bam}
#		"""

#rule indexSortedBam:
#	input:
#		'Bam/{tech}_dm6_trim_q5_dupsRemoved_sorted.bam'
#	output:
#		'Bam/{tech}_dm6_trim_q5_dupsRemoved_sorted.bam.bai'
#	params:
#		module = samtoolsVer
#	shell:
#		"""
#		module purge && module load {params.module}
#		samtools index {input}	
#		"""

#rule nameSortBam:
#	input:
#		'Bam/{tech}_dm6_trim_q5_dupsRemoved_sorted.bam'
#	output:
#		temp('Bam/{tech}_dm6_trim_q5_dupsRemoved_nameSorted.bam')
#	params:
#		module = samtoolsVer
#	shell:
#		"""
#		module purge && module load {params.module}
#		samtools sort -n {input} -o {output}
#		"""
#
#rule mergeBedFiles:
#	input:
#		lambda wildcards: getMergedBed(wildcards)
#		#getMergedBed
#	output:
#		'Bed/Merged/{merge}_dm6_trim_q5_dupsRemoved.bed'
#	#group: 'makeAndSplitBed'
#	shell:
#		"""
#		cat {input} | sort -k 1,1 -k 2,2n > {output}
#		"""
#rule makeMergedFragmentBedGraphs:
#	input:
#		dm = 'Bed/Merged/{sample}_dm_trim_q5_dupsRemoved_{fragType}.bed',
#		#spike = 'Bed/Merged/{sample}_sacCer_trim_q5_dupsRemoved.bed'
#	output:
#		unNorm = temp('BigWig/Merged/{sample}_dm_trim_q5_dupsRemoved_{fragType}.bg'),
#		rpgcNorm = temp('BigWig/Merged/{sample}_dm_trim_q5_dupsRemoved_{fragType}_rpgcNorm.bg')
#		#spikeNorm = temp('BigWig/Merged/{sample}_dm_trim_q5_dupsRemoved_{fragType}_spikeNorm.bg'),
#	params:
#		genomeSize = genomeSize,
#		chromSizePath = chromSizePath,
#		sampleName = '{sample}',
#		module = bedtoolsVer,
#		readLen = readLen
#	shell:
#		"""
#		module purge && module load {params.module}
#		#spikeCount=$(samtools view -c {input.spike})
#		readCount=$(wc -l {input.spike} | sed -e 's/^  *//' -e 's/  */,/g' | cut -d , -f 1)
#		readCount=$(wc -l {input.dm} | sed -e 's/^  *//' -e 's/  */,/g' | cut -d , -f 1)
#		spikeScale=$(echo "scale=5; 10000/${{spikeCount}}/" | bc)
#		rpgcScale=$(echo "scale=5; {params.genomeSize}/(${{readCount}} * {params.readLen})" | bc)
#		bedtools genomecov -i {input.dm} -bga -g {params.chromSizePath} > {output.unNorm}
#		bedtools genomecov -i {input.dm} -bga -g {params.chromSizePath} -scale ${{spikeCount}} > {output.spikeNorm}
#		bedtools genomecov -i {input.dm} -bga -g {params.chromSizePath} -scale ${{rpgcScale}} > {output.rpgcNorm}
#		"""

#rule convertMergedToBigWig:
#	input:
#		'BigWig/Merged/{sample}_dm_trim_q5_dupsRemoved_{fragType}{normType}.bg'
#	output:
#		'BigWig/Merged/{sample}_dm_trim_q5_dupsRemoved_{fragType}{normType}.bw'
#	params:
#		module = ucscVer,
#		chromSizePath = chromSizePath
#	shell:
#		"""
#		module purge && module load {params.module}
#		wigToBigWig {input} {params.chromSizePath} {output}
#		"""
#
#rule zNormMergedBigWig:
#	input:
#		'BigWig/Merged/{sample}_dm_trim_q5_dupsRemoved_{fragType}_rpgcNorm.bw'
#	output:
#		zNorm = 'BigWig/Merged/{sample}_dm_trim_q5_dupsRemoved_{fragType}_rpgcNorm_zNorm.bw',
#		zStats = 'Logs/Merged/{sample}_dm_trim_q5_dupsRemoved_{fragType}.zNorm'
#	params:
#		module = rVer,
#		srcDirectory = srcDirectory
#	shell:
#		"""
#		module purge && module load {params.module}
#		Rscript --vanilla {params.srcDirectory}/zNorm.r {input} {output.zNorm} > {output.zStats}
#		"""
#
#rule callMergedPeaks:
#	input:
#		'Bed/Merged/{sample}_dm_trim_q5_dupsRemoved_{fragType}.bed'
#	output:
#		'Peaks/Merged/{sample}_dm_trim_q5_dupsRemoved_{fragType}_peaks.narrowPeak'
#	params:
#		module = macsVer,
#		control = controlDNAPath,
#		prefix = 'Peaks/{sample}_dm_trim_q5_dupsRemoved_{fragType}',
#		genomeSize = genomeSize
#	shell:
#		"""
#		module purge && module load {params.module}
#		macs2 callpeak -f BEDPE -c {params.control} --broad -n {params.prefix} -g {params.genomeSize} -t {input} --nomodel --seed 123
#		macs2 callpeak -f BEDPE -c {params.control} -n {params.prefix} -g {params.genomeSize} -t {input} --nomodel --seed 123 
#		"""
#
##rule makeFragmentBedGraphs:
#	input:
#		dm = 'Bed/{sample}_dm_trim_q5_dupsRemoved_{fragType}.bed',
#		spike = 'Bam/{sample}_sacCer_trim_q5_dupsRemoved.bam'
#	output:
#		unNorm = temp('BigWig/{sample}_dm_trim_q5_dupsRemoved_{fragType}.bg'),
#		spikeNorm = temp('BigWig/{sample}_dm_trim_q5_dupsRemoved_{fragType}_spikeNorm.bg'),
#		rpgcNorm = temp('BigWig/{sample}_dm_trim_q5_dupsRemoved_{fragType}_rpgcNorm.bg')
#	params:
#		genomeSize = genomeSize,
#		chromSizePath = chromSizePath,
#		sampleName = '{sample}',
#		module = bedtoolsVer,
#		readLen = readLen
#	shell:
#		"""
#		module purge && module load {params.module}
#		spikeCount=$(samtools view -c {input.spike})
#		readCount=$(wc -l {input.dm} | sed -e 's/^  *//' -e 's/  */,/g' | cut -d , -f 1)
#		spikeScale=$(echo "scale=5; 10000/${{spikeCount}}/" | bc)
#		rpgcScale=$(echo "scale=5; {params.genomeSize}/(${{readCount}} * {params.readLen})" | bc)
#		bedtools genomecov -i {input.dm} -bga -g {params.chromSizePath} > {output.unNorm}
#		bedtools genomecov -i {input.dm} -bga -g {params.chromSizePath} -scale ${{spikeCount}} > {output.spikeNorm}
#		bedtools genomecov -i {input.dm} -bga -g {params.chromSizePath} -scale ${{rpgcScale}} > {output.rpgcNorm}
#		"""
#
#rule convertToBigWig:
#	input:
#		'BigWig/{sample}_dm_trim_q5_dupsRemoved_{fragType}{normType}.bg'
#	output:
#		'BigWig/{sample}_dm_trim_q5_dupsRemoved_{fragType}{normType}.bw'
#	params:
#		module = ucscVer,
#		chromSizePath = chromSizePath
#	shell:
#		"""
#		module purge && module load {params.module}
#		wigToBigWig {input} {params.chromSizePath} {output}
#		"""
#
#rule parseFlagStat:
#	input:
#		trim = expand('Logs/{tech}_dm6_trim.flagstat', tech = techList, species = speciesList), 
#		q5 = expand('Logs/{tech}_dm6_trim_q5.flagstat', tech = techList, species = speciesList),
#		dups = expand('Logs/{tech}_dm6_trim_q5_dupsRemoved.flagstat', tech = techList, species = speciesList),
#		#trim = expand('Logs/{align}_dm6_trim.flagstat', align = alignList, species = speciesList), 
#		#q5 = expand('Logs/{align}_dm6_trim_q5.flagstat', align = alignList, species = speciesList),
#		#dups = expand('Logs/{align}_dm6_trim_q5_dupsRemoved.flagstat', align = alignList, species = speciesList),
#	output:
#		"Logs/collected_flag_statfiles.csv"
#	params:
#		module = pythonVer,
#		srcDirectory = srcDirectory
#	shell:
#		"""
#		module purge && module load {params.module}
#		python3 {params.srcDirectory}/parseflagstat.py Logs/
#		"""
#
#		expand('Bam/{tech}_dm6_trim.bam', tech = techList, species = speciesList),
#		expand('Bam/{tech}_dm6_trim_q5.bam', tech = techList, species = speciesList),
#		expand('Bam/{tech}_dm6_trim_q5_sorted.bam', tech = techList, species = speciesList),
#		expand('Bam/{tech}_dm6_trim_q5_dupsRemoved.bam', tech = techList, species = speciesList),
#		expand('Bam/{tech}_dm6_trim_q5_dupsRemoved.bam.bai', tech = techList, species = speciesList),
#		expand('Bam/{tech}_dm6_trim_q5_dupsRemoved_sorted.bam', tech = techList, species = speciesList),
#		expand('Bam/{tech}_dm6_trim_q5_dupsRemoved_sorted.bam.bai', tech = techList, species = speciesList),
#		expand('Bam/{tech}_dm6_trim_q5_dupsRemoved_nameSorted.bam', tech = techList, species = speciesList),
#		expand('Bed/{tech}_dm6_trim_q5_dupsRemoved.bed', tech = techList, species = speciesList),
#		expand('Bed/{tech}_dm_trim_q5_dupsRemoved_{fragType}.bed', tech = techList, fragType = fragTypeList),
#		expand('BigWig/{tech}_dm_trim_q5_dupsRemoved_{fragType}{normType}.bw', tech = techList, fragType = fragTypeList, normType = normTypeList),
#		expand('BigWig/{tech}_dm_trim_q5_dupsRemoved_{fragType}_rpgcNorm_zNorm.bw', tech = techList, fragType = fragTypeList),
#		expand('Logs/{tech}_dm_trim_q5_dupsRemoved_{fragType}.zNorm', tech = techList, fragType = fragTypeList),
#		expand('Peaks/{tech}_dm_trim_q5_dupsRemoved_{fragType}_peaks.narrowPeak', tech = techList, fragType = fragTypeList),
#		expand('Plots/{tech}_dm_trim_q5_dupsRemoved_{fragType}_cumulativeDistPlot.png', tech = techList, fragType = fragTypeList),
#
